let interval = setInterval(timer, 1000);

function playInterval() {
    interval = setInterval(timer, 1000);
}

function timer() {
    let count = document.querySelector('.display');
    let varCount = count.textContent;
    if (varCount < 30) {
        count.textContent = Number(varCount)+1;
    } else {
        clearInterval(interval);
    }
}

function clearTimer() {
    clearInterval(interval);
    let count = document.querySelector('.display');
    count.textContent = 0;
}